import { describe, expect, it } from "@jest/globals";
import { DateTime } from "luxon";
import { parse } from "../src/parse";
import { TemporalEvent } from "../src/types/TemporalEvent";

describe("parse", () => {
	it("parses monthly at day of month", () => {
		const rrule =
			"DTSTART:20221010T070000Z\nDTEND:20221010T080000Z\nRRULE:FREQ=MONTHLY;BYMONTHDAY=3;WKST=SU";
		const dates = parse(
			rrule,
			{
				to: DateTime.fromObject({ year: 2022, month: 11, day: 10 }),
			},
			{ timezone: "Universal" }
		);

		expect(dates.length).toBe(1);
		expect(
			equalsEvent(dates.at(0), {
				start: DateTime.fromObject(
					{ year: 2022, month: 11, day: 3, hour: 7 },
					{ zone: "Universal" }
				),
				end: DateTime.fromObject(
					{ year: 2022, month: 11, day: 3, hour: 8 },
					{ zone: "Universal" }
				),
			})
		).toBeTruthy();
	});

	it("parses monthly by day of week", () => {
		const rrule = "DTSTART:20221010T070000Z\nDTEND:20221010T080000Z\nFREQ=MONTHLY;INTERVAL=1;BYDAY=1SU;UNTIL=20230531T000000Z";
	});
});

function equalsEvent(value?: TemporalEvent, other?: TemporalEvent) {
	console.log(value?.start.toObject());
	console.log(other?.start.toObject());
	return (
		other && value?.start.equals(other.start) && value.end.equals(other.end)
	);
}
