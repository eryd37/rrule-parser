"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parse = void 0;
const luxon_1 = require("luxon");
const RRuleDateToISO_1 = require("./RRuleDateToISO");
function parse(rruleString, interval, options) {
    console.log(rruleString);
    const dtStart = /DTSTART:.*\n/
        .exec(rruleString)
        ?.at(0)
        ?.replace("DTSTART:", "");
    const dtEnd = /DTEND:.*\n/.exec(rruleString)?.at(0)?.replace("DTEND:", "");
    const rrule = /RRULE:.*/.exec(rruleString)?.at(0)?.replace("RRULE:", "");
    console.log(rrule);
    const frequency = /FREQ=[A-Za-z]+/
        .exec(rrule ?? "")
        ?.at(0)
        ?.replace("FREQ=", "");
    console.log(frequency);
    const byMonthDay = /BYMONTHDAY=[0-9]+/
        .exec(rrule ?? "")
        ?.at(0)
        ?.replace("BYMONTHDAY=", "");
    console.log(byMonthDay);
    const weekStartAt = /WKST=[A-Za-z]+/
        .exec(rrule ?? "")
        ?.at(0)
        ?.replace("WKST=", "");
    console.log(weekStartAt);
    let startDate = undefined;
    let endDate = undefined;
    if (dtStart) {
        startDate = luxon_1.DateTime.fromISO((0, RRuleDateToISO_1.RRuleDateToISO)(dtStart), {
            zone: options?.timezone,
        });
    }
    if (dtEnd) {
        endDate = luxon_1.DateTime.fromISO((0, RRuleDateToISO_1.RRuleDateToISO)(dtEnd), {
            zone: options?.timezone,
        });
    }
    if (!startDate) {
        throw new Error("Start date undefined");
    }
    let iteratorDate = startDate;
    const maxDate = interval.to;
    const dates = [];
    while (iteratorDate
        .plus({ month: 1 })
        .startOf("month")
        .set({ day: Number(byMonthDay) }) < maxDate ||
        (endDate && iteratorDate < endDate)) {
        iteratorDate = iteratorDate
            .plus({ month: 1 })
            .startOf("month")
            .set({ day: Number(byMonthDay) });
        dates.push(iteratorDate);
    }
    console.log(dates);
    return dates;
}
exports.parse = parse;
