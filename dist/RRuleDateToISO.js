"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RRuleDateToISO = void 0;
function RRuleDateToISO(dateString) {
    if (!/[0-9]{8,8}T[0-9]{6,6}Z/.exec(dateString)?.length) {
        throw new Error("Date string did not match expected format");
    }
    const formatted = dateString.substring(0, 4) +
        "-" +
        dateString.substring(4, 6) +
        "-" +
        dateString.substring(6, 11) +
        ":" +
        dateString.substring(11, 13) +
        ":" +
        dateString.substring(13, 16);
    return formatted;
}
exports.RRuleDateToISO = RRuleDateToISO;
