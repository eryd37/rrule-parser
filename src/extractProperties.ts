import { DateTime } from "luxon";
import { RRuleDateToISO } from "./RRuleDateToISO";
import { mapFrequency } from "./mapFrequency";
import { RRuleFrequency } from "./types/RRuleFrequency";
import { RRuleProperties } from "./types/RRuleProperties";

export function extractProperties(
	rruleString: string,
	options?: { accountForDST?: boolean; timezone?: string }
): RRuleProperties {
	const dtStart = /DTSTART:[0-9A-Za-z]+/
		.exec(rruleString)
		?.at(0)
		?.replace("DTSTART:", "");
	const dtEnd = /DTEND:[0-9A-Za-z]+/
		.exec(rruleString)
		?.at(0)
		?.replace("DTEND:", "");

	const rrule =
		/RRULE:.*/.exec(rruleString)?.at(0)?.replace("RRULE:", "") ?? "";

	const frequency = mapFrequency(
		/FREQ=[A-Za-z]+/.exec(rrule)?.at(0)?.replace("FREQ=", "") as RRuleFrequency
	);
	const interval = Number(
		/INTERVAL=[0-9]+/.exec(rrule)?.at(0)?.replace("INTERVAL=", "")
	) || 1;

	const byMonthDay =
		Number(
			/BYMONTHDAY=[0-9]+/.exec(rrule)?.at(0)?.replace("BYMONTHDAY=", "")
		) || undefined;
	const byWeekDay = /BYDAY=[A-Za-z0-9]+/
		.exec(rrule)
		?.at(0)
		?.replace("BYDAY=", "");
	const weekStartsAt = /WKST=[A-Za-z]+/
		.exec(rrule)
		?.at(0)
		?.replace("WKST=", "");

	const count =
		Number(/COUNT=[0-9]+/.exec(rrule)?.at(0)?.replace("COUNT=", "")) ||
		undefined;
	const untilString = /UNTIL=[0-9A-Za-z]+/
		.exec(rrule)
		?.at(0)
		?.replace("UNTIL=", "");

	let startDate: DateTime | undefined = undefined;
	let endDate: DateTime | undefined = undefined;
	let until: DateTime | undefined = undefined;
	if (dtStart) {
		startDate = DateTime.fromISO(RRuleDateToISO(dtStart), {
			zone: options?.timezone,
		});
	}
	if (dtEnd) {
		endDate = DateTime.fromISO(RRuleDateToISO(dtEnd), {
			zone: options?.timezone,
		});
	}
	if (untilString) {
		until = DateTime.fromISO(RRuleDateToISO(untilString), {
			zone: options?.timezone,
		});
	}
	if (!startDate) {
		throw new Error("Start date undefined");
	} else if (!endDate) {
		throw new Error("End date undefined");
	}

	return {
		startDate,
		endDate,
		frequency,
		interval,
		byMonthDay,
		byWeekDay,
		weekStartsAt,
		count,
		until
	};
}
