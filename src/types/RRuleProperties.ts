import { DateTime } from "luxon";

export interface RRuleProperties {
	startDate: DateTime;
	endDate: DateTime;
	frequency: "day" | "week" | "month" | "year";
	interval: number;
	byMonthDay?: number;
	byWeekDay?: string;
	weekStartsAt?: string;
	count?: number;
	until?: DateTime;
}
