import { DateTime } from "luxon";

export interface TemporalEvent {
  start: DateTime;
  end: DateTime;
}
