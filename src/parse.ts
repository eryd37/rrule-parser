import { DateTime } from "luxon";
import { extractProperties } from "./extractProperties";
import { TemporalEvent } from "./types/TemporalEvent";

export function parse(
	rruleString: string,
	interval: { from?: DateTime; to: DateTime },
	options?: { accountForDST?: boolean; timezone?: string }
): TemporalEvent[] {
	const rruleProperties = extractProperties(rruleString, options);
  const eventDuration = rruleProperties.endDate.diff(rruleProperties.startDate);
	const maxDate = interval.to;

	const initialDate = interval.from ?? rruleProperties.startDate;
	let iteratorDate = initialDate.startOf(rruleProperties.frequency).set({
		day: rruleProperties.byMonthDay,
		hour: initialDate.hour,
		minute: initialDate.minute,
		second: initialDate.second,
		millisecond: initialDate.millisecond,
	});
	
	const dates: { start: DateTime; end: DateTime }[] = [];

	while (
		iteratorDate
			.plus({ [rruleProperties.frequency]: rruleProperties.interval })
			.startOf(rruleProperties.frequency)
			.set({ day: Number(rruleProperties.byMonthDay) }) < maxDate ||
		(rruleProperties.endDate && iteratorDate < rruleProperties.endDate)
	) {
		const endDate = iteratorDate.plus(eventDuration);
		dates.push({ start: iteratorDate, end: endDate });

		iteratorDate = iteratorDate
			.plus({ [rruleProperties.frequency]: rruleProperties.interval })
			.startOf(rruleProperties.frequency)
			.set({
				day: rruleProperties.byMonthDay,
				hour: initialDate.hour,
				minute: initialDate.minute,
				second: initialDate.second,
				millisecond: initialDate.millisecond,
			});
	}
	return dates;
}
