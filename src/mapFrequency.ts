import { Frequency } from "./types/Frequency";
import { RRuleFrequency } from "./types/RRuleFrequency";

export function mapFrequency(frequency: RRuleFrequency): Frequency {
	const map: Record<RRuleFrequency, Frequency> = {
		DAILY: "day",
		WEEKLY: "week",
		MONTHLY: "month",
		YEARLY: "year",
	};
	return map[frequency];
}
